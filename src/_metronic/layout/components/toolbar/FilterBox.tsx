/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { FC, useState, useEffect, useContext, Dispatch } from 'react';
import ReactSlider from 'react-slider'
import { ReactDatePicker } from 'react-datepicker'
import { addDays } from 'date-fns';
import "react-datepicker/dist/react-datepicker.css";
import DatePicker from "react-datepicker";

type Props = {
    exclude?: any[],
    setGender?: Dispatch<any>,
    setAge?: Dispatch<any>,
    setRanking?: Dispatch<any>,
    setSource?: Dispatch<any>
}

const FilterBox: React.FC<Props> = ({ exclude = [], setGender, setAge, setRanking, setSource }) => {
    const [branch, setBranch] = useState('1')
    const [date, setDate] = useState('1')

    var today = new Date();
    var tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 100);
    const [dateRange, setDateRange] = useState([null, null]);
    const [startDate, endDate] = dateRange;

    useEffect(() => {
    }, [branch, date])

    const debounce = (fn: Function, ms = 3000) => {
        let timeoutId: ReturnType<typeof setTimeout>;
        return function (this: any, ...args: any[]) {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(() => fn.apply(this, args), ms);
        };
    };


    const formatAge = (range: number[] = [], setAge: Dispatch<any>) => {
        const [init, end] = range;
        range = [];
        for (let x = init; x <= end; x++) {
            range.push(x);
        }
        setAge(range);
    };

    return <>
        {/* begin::Row */}
        <div className="row form-group mb-5 card">
            <div className="card-body">
                <div className="row">
                    {!exclude.includes('venue') &&
                        <div className="col-4 mb-4 ">
                            <label htmlFor="">Sucursal</label>
                            <select
                                className='form-select form-select-solid form-control'
                                data-kt-select2='true'
                                data-placeholder='Select option'
                                data-allow-clear='true'
                                defaultValue={'1'}
                                onChange={e => { setBranch(e.target.value) }}
                            >
                                <option></option>
                                <option value='1'>sucursal 1</option>
                                <option value='2'>sucursal 2</option>
                                <option value='3'>sucursal 3</option>
                                <option value='4'>sucursal 4</option>
                            </select>
                        </div>
                    }
                    {setGender &&
                        <div className="col-4 mb-4 ">
                            <label htmlFor="">Genero</label>
                            <select
                                className='form-select form-select-solid form-control'
                                data-kt-select2='true'
                                data-placeholder='Select option'
                                data-allow-clear='true'
                                onChange={(e: any) => setGender(e.target.value)}
                            >
                                <option value='M'>Masculino</option>
                                <option value='F'>Femenino</option>
                                <option value=''>Ambos</option>
                            </select>
                        </div>
                    }
                    {
                        !exclude.includes('date') &&
                        <div className="col-4 mb-4 ">
                            <label htmlFor="">Dia</label>
                            <DatePicker
                                className='form-control'
                                selectsRange={true}
                                startDate={startDate}
                                endDate={endDate}
                                onChange={(update: any) => {
                                    setDateRange(update);
                                }}
                                isClearable={true}
                            />
                        </div>
                    }
                    {setAge &&
                        <div className="col-4 mb-4 ">
                            <label htmlFor="">Edad</label>
                            <ReactSlider
                                className="horizontal-slider"
                                thumbClassName="example-thumb"
                                trackClassName="example-track"
                                defaultValue={[10, 100]}
                                ariaLabel={['Lower thumb', 'Upper thumb']}
                                ariaValuetext={state => `Thumb value ${state.valueNow}`}
                                renderThumb={(props, state) => <div {...props}>{state.valueNow}</div>}
                                pearling
                                minDistance={5}
                                onChange={(e: number[]) => (formatAge(e, setAge))}
                            />
                        </div>
                    }
                    {setRanking &&
                        <div className="col-4 mb-4 ">
                            <label htmlFor="">Ranking</label>
                            <input
                                type="number"
                                className="col-3 form-control"
                                onChange={((e: any) => setRanking(e.target.value))}
                            />
                        </div>
                    }
                    {setSource &&
                        <div className="col-4 mb-4 ">
                            <label htmlFor="">Fuente de datos</label>
                            <select
                                className='form-select form-select-solid form-control'
                                data-kt-select2='true'
                                data-placeholder='Select option'
                                data-allow-clear='true'
                                onChange={((e: any) => setSource(e.target.value))}
                            >
                                <option value='Facebook'>Facebook</option>
                                <option value='Form'>Formulario</option>
                                <option value=''>Todas</option>
                            </select>
                        </div>
                    }
                </div>
            </div>
        </div>
    </>
}

export { FilterBox }
