// Libraries
import React, { createContext, useEffect, useState } from "react";
import { sankey as d3sankey } from "d3-sankey";

// Library Types
import type { ReactNode } from "react";
import type { SankeyGraph, SankeyNode, SankeyLink } from "d3-sankey";

// Props
interface SankeyProps<N, L> {
  data: {
    nodes: SankeyNode<N, L>[];
    links: SankeyLink<N, L>[];
  };
  width?: number;
  height?: number;
  nodeWidth: number;
  nodePadding: number;
  children?: (sankey: { graph: SankeyGraph<N, L> }) => ReactNode;
}

function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: 0,
    height: 0,
  });
  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    // Add event listener
    window.addEventListener("resize", handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty array ensures that effect is only run on mount
  return windowSize;
}
// Component
export default function Sankey<N, L>({
  data,
  width,
  height,
  nodeWidth,
  nodePadding,
  children
}: SankeyProps<N, L>) {
  // Handling Size
  const { width: windowWidth, height: windowHeight } = useWindowSize();

  const sankeyWidth = width ? width : windowWidth - 700;
  const sankeyHeight = height ? height : windowHeight - 100;

  // State & Data
  const [graph, setGraph] = useState<SankeyGraph<N, L>>({
    nodes: [],
    links: []
  });

  useEffect(() => {
    if (data.links.length) {
      setGraph(
        d3sankey<N, L>()
          .nodeWidth(nodeWidth)
          .nodePadding(nodePadding)
          .extent([
            [0, 10],
            [sankeyWidth, sankeyHeight - 50]
          ])(data)
      );
    }

  }, [nodePadding, nodeWidth, sankeyWidth, sankeyHeight, data]);

  useEffect(() => {
    setGraph(
      d3sankey<N, L>()
        .update(data)
    );

  }, [data])
  if (children)
    return (
      <svg width={sankeyWidth} height={sankeyHeight}>
        {children({ graph })}
      </svg>
    );

  return <g />;
}
