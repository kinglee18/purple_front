export type ExtraNodeProperties = {
    name: string;
    origin?: string;
    description?: string;
    type: string;
  };

export type ExtraLinkProperties = {
    destinationNodeName: string
}
  