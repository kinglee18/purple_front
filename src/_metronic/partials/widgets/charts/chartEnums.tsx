enum ChartValueType {
    Raw = "Raw",
    Smoothed = "Smoothed",
    Corrected = "Corrected",
    CorrectedSmoothed = "Corrected Smoothed",
    Deltas = "Deltas",
    DeltasSmoothed = "Deltas Smoothed"
  }
  
  enum ChartHorizontalAxisType {
    None = 0,
    Timestamp = 1,
    Custom = 2
  }
  
  export { ChartValueType, ChartHorizontalAxisType };
  