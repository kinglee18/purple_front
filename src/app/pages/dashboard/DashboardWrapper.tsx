/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { FC, useState, useEffect } from 'react'
import { useIntl } from 'react-intl'
import { PageTitle } from '../../../_metronic/layout/core';
import { Sankey, Link, Node } from '../../../_metronic/partials/widgets/charts/Sankey'
import type { ExtraNodeProperties } from '../../../_metronic/partials/widgets/charts/Sankey/types'
import * as d3 from "d3";
import chroma from "chroma-js";
import { FilterBox } from '../../../_metronic/layout/components/toolbar/FilterBox';


const DashboardPage: FC = () => {
  const [dta, setDta] = useState({ links: [], nodes: [] });
  useEffect(() => {
    fetch('https://mrmecehqg3.execute-api.us-east-1.amazonaws.com/dev/GetDashData?venue=20180&year=2020&month=02'
      , {
        method: "GET",
        headers: {},
      }).then(response => response.json())
      .then(data => setDta(data)).catch(e => {
        console.error(e);
      });
  }, [])


  return <>
    <FilterBox/>
    <div className='row gy-5 g-xl-8'>
      <div className='col-12 mt-5'>
        <Sankey<ExtraNodeProperties, {}>
          data={dta}
          nodeWidth={100}
          nodePadding={40}
        >
          {({ graph }) => {

            const color = chroma.scale("Set2").classes(graph.nodes.length);
            const colorScale = d3
              .scaleLinear()
              .domain([0, graph.nodes.length])
              .range([0, 1]);

            return (
              <g>
                {graph &&
                  graph.links.map((link: any, i) => (
                    <Link
                      key={`sankey-link-${i}`}
                      link={link}
                      color={color(colorScale(link.source.index)).hex()}
                      maxWidth={30}
                    />
                  ))}
                {graph &&
                  graph.nodes.map((node, i) => (
                    <Node<ExtraNodeProperties, {}>
                      key={`sankey-node-${i}`}
                      link={node}
                      color={color(colorScale(i)).hex()}
                      name={node.name}
                      height={30}
                      graph={graph}
                    />
                  ))}
              </g>
            );
          }}
        </Sankey>
      </div>
    </div>
  </>
}

const DashboardWrapper: FC = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({ id: 'MENU.DASHBOARD' })}</PageTitle>
      <DashboardPage />
    </>
  )
}

export { DashboardWrapper }
