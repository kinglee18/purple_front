/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { FC, useState, useEffect } from 'react'
import { useIntl } from 'react-intl'
import { PageTitle } from '../../../_metronic/layout/core';
import { SyncedValueChart, SyncedValueChartSeries } from '../../../_metronic/partials/widgets/charts/SynchoronizdChart';
import { ChartHorizontalAxisType, ChartValueType } from '../../../_metronic/partials/widgets/charts/chartEnums';
import moment from 'moment';
import { FilterBox } from '../../../_metronic/layout/components/toolbar/FilterBox';
import { TablesWidget1, TablesWidget12, TablesWidget2, TablesWidget4 } from '../../../_metronic/partials/widgets';

const DashboardPage: FC = () => {
  const [chartData, setChartData] = useState<any>([]);
  const [age, setAge] = useState<any>([96]);
  const [gender, setGender] = useState<string>();
  const [ranking, setRanking] = useState<string>();
  const [source, setSource] = useState<string>();
  type Params = {
    age: string,
    gender?: string,
    ranking?: string,
    source?: string,
  }

  let url = 'https://anul367cu1.execute-api.us-east-1.amazonaws.com/default/get-recommendations';

  useEffect(() => {
    let params: Params = {
      age: age.toString(),
      gender: gender,
      ranking: ranking,
      source: source
    }




    Object.keys(params).forEach(key => {
      const a = params[key as keyof Params];
      if (typeof a === 'undefined'){
        delete params[key as keyof Params];
      }
    })

    let urlParams = new URLSearchParams();
    Object.entries(params).forEach(([key, value]) => {
      urlParams.set(key, value);
    })
    url += '?' + urlParams.toString();
    fetch(url, {
      method: "GET",
      headers: {},
    }).then(response => response.json())
      .then(data =>
        setChartData(data)
      ).catch(e => {
        console.error(e);
      });
  }, [age, gender, ranking, source]);


  useEffect(() => {


  }, [chartData])

  return <>
    <FilterBox exclude={['venue', 'date']} setAge={setAge} setGender={setGender} setRanking={setRanking} setSource={setSource} />
    <div className='row mb-3'>
      <TablesWidget12 data={chartData} className='card-xl-stretch mb-xl-8'></TablesWidget12>
    </div>

  </>
}

const Recommendations: FC = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({ id: 'MENU.DASHBOARD' })}</PageTitle>
      <DashboardPage />
    </>
  )
}

export { Recommendations }
