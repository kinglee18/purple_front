/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { FC, useState, useEffect } from 'react'
import { useIntl } from 'react-intl'
import { PageTitle } from '../../../_metronic/layout/core';
import { SyncedValueChart, SyncedValueChartSeries } from '../../../_metronic/partials/widgets/charts/SynchoronizdChart';
import { ChartHorizontalAxisType, ChartValueType } from '../../../_metronic/partials/widgets/charts/chartEnums';
import moment from 'moment';
import { FilterBox } from '../../../_metronic/layout/components/toolbar/FilterBox';
moment().format();

const DashboardPage: FC = () => {
  const [chartData, setChartData] = useState<any>([]);
  const [branch, setBranch] = useState('1')
  const [date, setDate] = useState('1')


  useEffect(() => {
    fetch('https://gneojrjl7f.execute-api.us-east-1.amazonaws.com/default/get-metrics?date_start=2020-12-01&date_end=2021-12-04&venue_id=17773'
      , {
        method: "GET",
        headers: {
        },

      }).then(response => response.json())
      .then(data =>
        getSeries(data)
      ).catch(e => {
        console.error(e);
      });

  }, [])


  useEffect(() => {


  }, [chartData])

  const getSeries = (data: Array<any>): void => {
    const topics = ["Potencia_R", "Nuevos_R", "Potencia_nR", "Nuevos_nR", "Clientes_Frec_R", "Clientes_Frec_nR"];
    const chart = topics.map(topic => ({
      name: topic,
      values: data.map(val => [(new Date(val.Fechas).getTime()), val[topic]])
    }));
    setChartData(chart);
  };
  return <>
    <FilterBox />

    <div className='row gy-5 g-xl-8'>
      <div className='col-12 mt-5'>
        <SyncedValueChart
          series={chartData}
          valueTypes={[ChartValueType.Raw]}
          horizontalAxisType={ChartHorizontalAxisType.Custom}
          showMarkers={true}
          title=""
        />
      </div>
    </div>
  </>
}

const UsersBehaviour: FC = () => {
  const intl = useIntl()
  return (
    <>
      <PageTitle breadcrumbs={[]}>{intl.formatMessage({ id: 'MENU.DASHBOARD' })}</PageTitle>
      <DashboardPage />
    </>
  )
}

export { UsersBehaviour }
