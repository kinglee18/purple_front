/* eslint-disable jsx-a11y/anchor-is-valid */
import clsx from 'clsx'
import React, { useState } from 'react'
import { KTSVG } from '../../../_metronic/helpers'
import { getLayout, ILayout, LayoutSetup, useLayout } from '../../../_metronic/layout/core'
import { Link } from 'react-router-dom'

const BuilderPage: React.FC = () => {
  const { setLayout } = useLayout()
  const [tab, setTab] = useState('Header')
  const [config, setConfig] = useState<ILayout>(getLayout())
  const [configLoading, setConfigLoading] = useState<boolean>(false)
  const [resetLoading, setResetLoading] = useState<boolean>(false)

  const updateData = (fieldsToUpdate: Partial<ILayout>) => {
    const updatedData = { ...config, ...fieldsToUpdate }
    setConfig(updatedData)
  }

  const updateConfig = () => {
    setConfigLoading(true)
    try {
      LayoutSetup.setConfig(config)
    } catch (error) {
      setConfig(getLayout())
    }
    setTimeout(() => {
      setLayout(config)
      setConfigLoading(false)
    }, 1000)
  }

  const reset = () => {
    setResetLoading(true)
    setTimeout(() => {
      setConfig(getLayout())
      setResetLoading(false)
    }, 1000)
  }

  return (
    <>
      <div className="card">
        <div className="card-body">
          <input
            type='text'
            className='form-control '
            name='compose_cc'
            placeholder="Buscar cliente"
          />
          <table className="align-middle fs-6 gy-5 no-footer table table-row-dashed">
            <thead>
              <tr className="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                <th>Nombre</th>
                <th>Habilitado</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <Link to='/client'>
                    Cliente 1
                  </Link>
                </td>
                <td>
                  <input
                    className='form-check-input'
                    type='checkbox'
                    name='notifications'
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <Link to='/client'>
                    Cliente 2
                  </Link>
                </td>
                <td>
                  <input
                    className='form-check-input'
                    type='checkbox'
                    name='notifications'
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

    </>
  )
}

export { BuilderPage }
